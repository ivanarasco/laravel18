<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* SIEMPRE VAMOS A PONER EL PRIMER ELEMENTO ('users') EN PLURAL.
   ÉSTO SE HARÁ POR QE HAY UN ESTÁNDAR DE FACTO LLAMADO "REST"
*/

// Route::get('users', 'UserController@index')->name('usuarios'); // alias que le das a las rutas->(name('usuarios'))

// //ojo al orden, si ponemos lo siguiente al revés, irá al id "create", pues considera "create" como parámetro.
// Route::get('users/create', 'UserController@create');
// Route::get('users/{id}', 'UserController@show');
/*
   EL ESTÁNDAR REST DICE QUE CUANDO RECIBES LOS DATOS DE ALGO, QUE SEA JUSTO DESPUÉS DEL PLURAL. (users/5)
   y no: users/show/5.
*/
Route::get('users/especial', 'UserController@especial');
// recordar siempre poner los métodos que queramos que haga en caso especial por encima de los genéricos.
Route::resource('users', 'UserController');
Route::resource('products', 'ProductController');

// Route::get('users/{id}', function ($id){
//     return "Detalle del usuario $id";
// })->where('id', '[0-9]+'); // con esta condición le digo que el id sea numérico, sino no entra.

// Route::get('users/{id}/{name}', function ($id, $name){
//     return "Detalle del usuario $id. El nombre es $name";
// });

// Route::get('users/{id}/{name?}', function ($id, $name=null){
// //name? significa "que si está o no está." para enlazar con if($name)
//     if ($name){
//         return "Detalle del usuario $id. El nombre es $name";
//     }
//     return "Detalle del usuario $id. Es Anónimo.";
// });
