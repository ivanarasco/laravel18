@extends('layouts.app')

@section('title','Lista de Usuarios')

@section('content')
<h2> Lista de Usuarios </h2>
{{-- <h4> Recorriendo sin comprobar</h4> --}}
<a href=/users/create> Nuevo </a>
<ul>
    @forelse ($users as $user)
    <li>Nombre: {{$user->name}} | Correo: {{$user->email}} |
        <a href="users/{{ $user->id}}/edit"> Editar </a>
        -
        <a href="users/{{ $user->id }}"> Detalle </a>
        -
        <form method="post" action="/users/{{ $user->id }}">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" value="Borrar">
        </form>

    </li>

    @empty {{-- else --}}

    <p> No hay usuarios</p>
    @endforelse
</ul>
<hr>
{{ $users->render() }} {{-- para ver las páginas --}}
@endsection

{{-- <h4> Usando "if"</h4>
    @if (!empty($users))
        @foreach ($users as $user)
             <li>{{$user}} </li>
        @endforeach
    @else
        <p> ¡No hay usuarios! </p>
    @endif
<hr>
<h4> Usando "for-else"</h4>

    @forelse ($users as $user)
        <li>{{$user}} </li>
    @empty
        <p> ¡No hay usuarios! </p>
    @endforelse
    --}}

