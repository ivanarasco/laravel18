<?php

namespace Tests\Feature;

use Tests\TestCase; //esta si es una clase
use App\User; // esta tmbn
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
/* No son clases estas 2. */

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    // LE PODEMOS AÑADIR CUALIDADES A UNA CLASE.

    use RefreshDatabase; // cada test se ejecutará sobre una base de datos limpia, le hemos dicho que vaya a otra.

    /* @group */

    public function test_lista_de_usuarios_vacia()
    {
      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertSee('Lista de Usuarios');
      $response->assertSee('No hay usuarios');
  }

  public function test_lista_de_usuarios()
  {

      factory(User::class)->create([
          'name' => 'Pepe',
          'email' => 'pepe@gmail.com'
      ]);

      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertDontSee('No hay usuarios');
      $response->assertSee('Lista de Usuarios');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
  }

  public function test_metodo_show_user_1()
  {
      factory(User::class)->create([
          'id' => 1,
          'name' => 'Pepe',
          'email' => 'pepe@gmail.com'
      ]);

      $response = $this->get('/users/1');
      $response->assertStatus(200);
      $response->assertSee('Este es el detalle del usuario 1');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
  }

  public function test_metodo_show_user_inexistente()
{
    $response = $this->get('/users/100000');
    $response->assertStatus(404);
}


    // public function test_lista_de_usuarios()
    // {
    //     $response = $this->get('/users');
    //     $response->assertStatus(200);
    //     $response->assertSee("Lista de Usuarios");
    //     $response->assertSee("Pepe");
    //     // $response->assertSee("Juan");
    // }
    //  public function test_metodo_show_user_2()
    // {
    //     $response = $this->get('/users/2');
    //     $response->assertStatus(200);
    //     $response->assertSee("Detalle de usuario");
    //     $response->assertSee("Pepe");

    // }
}
