<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
   public function test_lista_productos()
    {
        $response = $this->get('/products');
        $response->assertStatus(200);
        $response->assertSee("Lista de Productos");
        $response->assertSee("Aut non");
        // $response->assertSee("Juan");
    }
     public function test_metodo_show_product_1()
    {
        $response = $this->get('/products/1');
        $response->assertStatus(200);
        $response->assertSee("Detalle de Producto");
        $response->assertSee("Aut non");

    }
}
