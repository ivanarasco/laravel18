@extends('layouts.app') {{-- carpeta.fichero --}}

{{-- A continuación, se desarrolla el código a partir de la estructura básica. --}}

@section('title', 'de Productos')

@section('cabecera')
        <h2> Lista de Productos </h2>
@endsection

@section('content')
    <ul>
        @foreach ($products as $product)
             <li>{{$product->name}} <a href="products/<?php echo $product->id ?>"> Detalle </a> </li>
        @endforeach
    </ul>
@endsection
