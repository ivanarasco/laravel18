<!DOCTYPE html> {{-- Definimos la estructura general. Es una plantilla de plantillas. --}}
<html>
<head>
    <title>@yield('title')</title>
</head>
<body>
    @section('cabecera')

    @show

    @section('content')


    @show

</body>
</html>
