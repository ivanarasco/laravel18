<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$users = array();
        // $users = array('Juan', 'Ana', 'Diego', 'Sandra');

        $products = Product::all();
        return view('product.index', ['products' => $products]);

        // busca el fichero, unos de los dos:
        // /resources/views/user/index.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "Formulario de registro";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)

    {
        // return view('user.show', [
        //     'id' => $id,
        //     'otro' => "otra variable"
        // ]);
        // -----> la otra manera

        $product =  Product::find($id);
        return view('product.show', [
            'product' => $product,
        ]);

        //return view('user.show', compact('user'));
                                 // el primer parámetro que recibe compact es el que recibe show.
                                 // le pasamos un array con todas las variables que nos interesan. Debería tener el nombre que queremos usar en la vista.
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function especial(){
        return redirect('/products');
        //return "soy especial";
    }
}
