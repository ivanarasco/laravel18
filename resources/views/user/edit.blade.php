@extends('layouts.app')

@section('title','Edición de Usuario')

@section('content')
<style type="text/css">
.alert {
    color:red;
}
.error{
    color:red;
}

</style>

<h2> Edición de Usuario </h2>

@if ($errors->any())
<div class="alert alert-danger">
    <hr>
    Se han producido errores de validación:
    <hr>
   {{-- <ul>

         @foreach ($errors->all() as $error)
            <li> {{ $error }} </li>
        @endforeach
    </ul>--}}

</div>
@endif

<form method="post" action="/users/{{$user->id}}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT"> <!-- "falsear" el formulario para que haga el "PUT" pues el mthod del form no debe ser "post" sino "put" por la especificación del framework. -->

    <label> Nombre </label>
    <input type="text" name="name" value="{{ old('name') ? old('name') : $user->name}}">
    <div class="error">
        {{ $errors->first('name') }}
    </div>
    <br><br>
    <label> Email </label>
    <input type="text" name="email" value="{{ old('email') ? old('email') : $user->email}}">
    <div class="error">
        {{ $errors->first('email') }}
    </div>
    <br><br>
    <label> Contraseña </label>
    <input type="password" name="password">
    <div class="error">
        {{ $errors->first('password') }}
    </div>

    <br><br>

    <input type="submit" name="Nuevo">
</form>

@endsection
