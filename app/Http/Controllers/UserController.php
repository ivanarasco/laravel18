<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$users = array();
        // $users = array('Juan', 'Ana', 'Diego', 'Sandra');

        //$users = User::all();
        $users = User::paginate(5);
        return view('user.index', ['users' => $users]);

        // busca el fichero, unos de los dos:
        // /resources/views/user/index.php
         // /resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return "Formulario de registro";
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) // tenemos el $_REQUEST en $request
    {
        // HACER ALTA DE USUARIO //

        // VISUALIZACIÓN DEL CONTENIDO RECIBIDO. PODEMOS FILTRARLO. //
        // dd($request);
        // dd($request->all());
        // dd($request->name);
        // dd($request->name, $request->password);
        // dd($request->only(['name', 'password']));

        // opcion1:

        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));
        // $user->remember_token = str_random(10);
        // $user->save();

        // opcion2:
        //$user = User::create($request->all());

        // opcion3:

            // manera 1:

        $rules = [
            'name' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|max:255|email',
             // que no se duplique el email (unique:users -> único en usuarios)
             // "email" como tal es para que sea un email válido.
            'password' => 'required|max:255',
            'color' => 'required',
        ];

        $request->validate($rules);

            // manera 2:

        // $request->validate([
        //     'name' => 'required',
        //     'email' => 'required',
        //     'password' => 'required',

        // ]);

        $user = new User();
        $user->fill($request->all()); // FILLABLE. Pero la password no se encripta.
        $user->save();


        return redirect('/users'); // como el header location

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) // me realiza el FIND Automáticamente.

    {
        /* return view('user.show', [
             'id' => $id,
             'otro' => "otra variable"
         ]);

          la otra manera:

         $user =  User::findOrFail($id);
           if($user == null){ -> generar un error por tu cuenta
               abort(404);
           }*/

           return view('user.show', [
            'user' => $user, // llevo el usuario entero.
        ]);

        //return view('user.show', compact('user'));
                                 // el primer parámetro que recibe compact es el que recibe show.
                                 // le pasamos un array con todas las variables que nos interesan. Debería tener el nombre que queremos usar en la vista.
       }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $rules = [
            'name' => 'required|unique:users,name,$id,id|max:255|min:4',
            'email' => "required|unique:users,email,$id,id|max:255|email",
             // que no se duplique el email (unique:users -> único en usuarios)
             // "email" como tal es para que sea un email válido.
             // unique:users,id,$id --> el id del usuario de ese email debe ignorarlo, para que pueda no editarlo si quiere, xq sino le dirá que está repe.
        ];

        $request->validate($rules);


        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();

        return redirect('/users/'. $user->id); // como el header location
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        //User::destroy($id) --> otro modo

        return redirect('/users');
        //return back(); que vuelva a la vista de la que viene, en este caso, igual que users.
    }

    public function especial(){ //coger un grpo determinado de usuarios.

        $users = User::where('id', '>=', 30)->where('id', '<=', 40)->get(); // usuarios del id 5 hasta el 10.
        dd($users);
        return "especial";
        return redirect('/users');
        //return "soy especial";
    }
}
