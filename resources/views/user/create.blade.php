@extends('layouts.app')

@section('title','Registro de Usuario')

@section('content')
<style type="text/css">
.alert {
    color:red;
}
.error{
    color:red;
}

</style>

<h2> Registro de Usuario </h2>

@if ($errors->any())
<div class="alert alert-danger">
    <hr>
    Se han producido errores de validación:
    <hr>
   {{-- <ul>

         @foreach ($errors->all() as $error)
            <li> {{ $error }} </li>
        @endforeach
    </ul>--}}

</div>
@endif

<form method="post" action="/users">
    {{ csrf_field() }}
    <label> Nombre </label>
    <input type="text" name="name" value="{{ old('name') }}">
    <div class="error">
        {{ $errors->first('name') }}
    </div>
    <br><br>
    <label> Email </label>
    <input type="text" name="email" value="{{ old('email') }}">
    <!-- recupera el valor del email, porque al validar, se borraría, así que así lo recuperamos -->
    <div class="error">
        {{ $errors->first('email') }}
    </div>
    <br><br>

    <?php
    $example = ['rojo', 'azul', 'verde']
    ?>

    <select name="color">
        @foreach ($example as $item)
        <option value="{{ $item }}"
        "{{ old('color') == $item ? 'selected="selected"' : ''}}">
        {{ $item }}
    </option>
    @endforeach

</select>
    {{-- {{ old('color') }} --}}
<br>

<label> Contraseña </label>
<input type="password" name="password">
<div class="error">
    {{ $errors->first('password') }}
</div>
<br><br>



<input type="submit" name="Nuevo">
</form>

@endsection
